//
//  CountriesController.swift
//  
//
//  Created by Jose Letona on 9/6/22.
//

import Vapor
import Fluent

struct CountriesController: RouteCollection {
    func boot(routes: RoutesBuilder) throws {
        let countriesRoute = routes.grouped("api", "countries")
        
        countriesRoute.get(use: getAll)
        countriesRoute.post(use: create)
        countriesRoute.put(use: update)
        countriesRoute.delete(":countryID", use: delete)
    }
    
    //Read all /api/countries -> 200
    func getAll(req: Request) throws -> EventLoopFuture<[Country]> {
        Country.query(on: req.db).all()
    }
    
    // Create /api/countries -> Body -> 200
    func create(req: Request) throws -> EventLoopFuture<HTTPStatus> {
        let country = try req.content.decode(Country.self)
        
        return country.save(on: req.db).transform(to: .ok)
    }
    
    // Update /api/countries -> Body -> 200
    func update(req: Request) throws -> EventLoopFuture<HTTPStatus> {
        let country = try req.content.decode(Country.self)

        return Country
            .find(country.id, on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap {
                $0.name = country.name

                return $0.update(on: req.db).transform(to: .ok)
            }
    }
    
    // Delete /api/coutries/countryID -> 200
    func delete(req: Request) throws -> EventLoopFuture<HTTPStatus> {
        Country
            .find(req.parameters.get("countryID"), on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap { $0.delete(on: req.db) }
            .transform(to: .ok)
    }
}

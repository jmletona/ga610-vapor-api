//
//  Country.swift
//  
//
//  Created by Jose Letona on 9/6/22.
//

import Vapor
import Fluent

final class Country: Model, Content {
    static let schema = "countries"
    
    @ID(key: .id)
    var id: UUID?
    
    @Field(key: "name")
    var name: String
    
    init() {}
    
    init(id: UUID? = nil,
         name: String) {
        self.id = id
        self.name = name
    }
}

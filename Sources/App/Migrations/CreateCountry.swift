//
//  CreateCountry.swift
//  
//
//  Created by Jose Letona on 9/6/22.
//

import Fluent

struct CreateCountry: Migration {
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        database.schema("countries")
            .id()
            .field("name", .string, .required)
            .unique(on: "name")
            .create()
    }
    
    func revert(on database: Database) -> EventLoopFuture<Void> {
        database.schema("countries").delete()
    }
}
